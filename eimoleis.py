#!/usr/bin/env python
# Einmaleins
# (2017,19 YCB)
print("# Einmaleins")
print()
for z in range(10):
# z=0,1,..,9
 print("\t", end='')
 for s in range(10):
# s=0,1,..,9
  fact=(z+1)*(s+1)
# repr(): Darstellung; .rjust(3): Rechtsausrichten auf Breite 3
  print(repr(fact).rjust(3), end='')
 print()
print()
