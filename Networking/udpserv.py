# simple UDP server

from socket import *

host='127.0.0.1'
host=''
port=2345
buf=1024

s=socket(AF_INET,SOCK_DGRAM)
s.bind( (host,port) )
data,addr=s.recvfrom(buf)
if not data:
	print('no data from',addr[0],addr[1])
else:
	print('\nData',data.decode('ascii'),'from',addr[0],addr[1])
	print('data type:',type(data.decode('ascii')))
s.close()
