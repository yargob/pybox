#!/usr/bin/env python
# (2017,19 YCB)
print("# printable ASCII")
print()
print('\t  /',0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f')
for z in range(2,8):
 print('\t',z,' ', end='')
 for s in range(16):
  print(chr(16*z+s)+' ', end='')
 print()
print()
